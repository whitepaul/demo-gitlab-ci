import numpy as np
import pandas as pd
import streamlit as st
import matplotlib.pyplot as plt


def main():
    st.title("My Streamlit App")

    # Add components
    st.header("Welcome to my app!")
    st.subheader("Here are some components you can use:")

    # Text input
    name = st.text_input("Enter your name:")
    st.write("Hello,", name)

    # Slider
    age = st.slider("Select your age:", 0, 100, 25)
    st.write("Your age is:", age)

    # Checkbox
    option = st.checkbox("Enable option")
    if option:
        st.write("Option is enabled!")
    else:
        st.write("Option is disabled!")

    # Create plots
    st.header("Plots")

    # Line plot
    x = np.linspace(0, 10, 100)
    y = np.sin(x)
    plt.plot(x, y)
    st.pyplot(plt)

    # Bar plot
    data = pd.DataFrame({'Category': ['A', 'B', 'C'], 'Value': [1, 2, 3]})
    plt.bar(data['Category'], data['Value'])
    st.pyplot(plt)


if __name__ == "__main__":
    main()
